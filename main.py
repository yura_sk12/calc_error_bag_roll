from bag_roll import BagRoll
from matplotlib import pyplot as plt

DATA_FILE = "param.txt"

if __name__ == '__main__':
    file = open(DATA_FILE, 'r', encoding="utf-8")

    flag = False
    bag_roll_dict = {}  # список с данными рулона
    bagRoll_list = []  # массив из классов BagRoll

    for roll in file.read().split("##"):
        if roll:
            for i in roll.split("\n"):
                if i:
                    bag_roll_dict[i.split(':')[0]] = eval(i.split(':')[1])
            else:
                bagRoll_list.append(BagRoll(bag_roll_dict))
                bag_roll_dict = {}

    print("Прочитано - ", len(bagRoll_list))
    tmp_lst = []
    tmp_lst2 = []
    погрешность_list = []
    for i in bagRoll_list:
        tmp_lst.append(i.get_длину_через_нарезанное())
        tmp_lst2.append(i.длина_рулона_м)
        погрешность_list.append(100 - i.get_длину_через_нарезанное()[1])

    # рисуем погрешность
    plt.plot(погрешность_list)

    # plt.plot(range(len(tmp_lst)), tmp_lst,
    #          range(len(tmp_lst)), tmp_lst2)
    plt.show()
